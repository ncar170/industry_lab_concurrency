package ictgradschool.industry.lab_concurrency.ex01;

import ictgradschool.Keyboard;

public class Exercise01 {

    public void start(){

        Exercise01Runnable myRunnable = new Exercise01Runnable();
        Thread thread = new Thread(myRunnable);

        System.out.println("About to start thread...");

        thread.start();

        System.out.println("Thread started successfully.");

        System.out.println();
        System.out.println("Press ENTER to stop the thread...");
        Keyboard.readInput();

        thread.interrupt();

        try {
            thread.join();
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        System.out.println("Done");

        System.out.println("Return: " + myRunnable.getCount());

    }


    public static void main(String[] args) {
        Exercise01 ex1 = new Exercise01();
        ex1.start();
    }


}
