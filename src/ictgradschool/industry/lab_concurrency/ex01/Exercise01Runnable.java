package ictgradschool.industry.lab_concurrency.ex01;

public class Exercise01Runnable implements Runnable {

    private int count = 0;

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()){
            for (int i = 0; i <= 100000; i++){
                System.out.println(i);
            }
        }
    }

    public int getCount() {
        return count;
    }
}
