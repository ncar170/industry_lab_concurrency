package ictgradschool.industry.lab_concurrency.ex04;

import ictgradschool.industry.lab_concurrency.examples.example03.IComputeTask;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class BankConsumer implements Runnable {

    private BlockingQueue<Transaction> queue;
    private BankAccount account;

    public BankConsumer(BlockingQueue <Transaction> queue) {
        this.queue = queue;
    }


    @Override
    public void run() {

        try {
            while (!Thread.currentThread().isInterrupted()){
                Transaction task = queue.take();

                 account = new BankAccount();

                    switch (task._type) {
                        case Deposit:
                            account.deposit(task._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(task._amountInCents);
                            break;
                    }

                System.out.println("Final balance: " + account.getFormattedBalance());
                }

        } catch (InterruptedException e){
            System.out.println(Thread.currentThread().getName() + " was interrupted and is quitting gracefully.");
        }


    }
}
