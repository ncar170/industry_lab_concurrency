package ictgradschool.industry.lab_concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {


    public void start (){

       final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);

       Thread producer = new Thread(new Runnable() {
           @Override
           public void run() {
               List<Transaction> transactions = TransactionGenerator.readDataFile();
               try {
                   for (Transaction t : transactions)
                       queue.put(t);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
       });
       producer.start();

//        try {
//            producer.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        List<Thread> consumers = new ArrayList<>();
       for (int i = 0; i < 2; i++){
           BankConsumer c = new BankConsumer(queue);
           Thread t = new Thread(c, "Consumer #" + i);
           t.start();
           consumers.add(t);
        }

        for (Thread t : consumers) {
          //t.interrupt();
            try {
                t.join();
//                System.out.println("Final balance: " + account.getFormattedBalance());
                System.out.println(t.getName() + " finished successfully.");
            } catch (InterruptedException e) {
                System.out.println("Interrupted Exception - BankingApp");
            }
        }

    }


    public static void main(String[] args) {
        ConcurrentBankingApp p = new ConcurrentBankingApp();
        p.start();
    }

}
