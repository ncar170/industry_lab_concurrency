package ictgradschool.industry.lab_concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) throws InterruptedException {
        // TODO Implement this.

        final double[] estimatedPi = {0};

        final double section = numSamples / 20;

        final double[] avgSamples = new double[5];

//        for (int i = 0; i < 10; i++){
//
//        }

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                    ThreadLocalRandom tlr = ThreadLocalRandom.current();
                    long numInsideCircle = 0;

                    int count = 0;

                    for (long i = 0; i < section; i++) {

                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();

                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle++;
                        }

                        count++;
                    }
                    estimatedPi[0] += 4.0 * (double) numInsideCircle / (double) section;
                }
            });
            threads.add(t);
        }

        // Run all the threads
        for (Thread t : threads) {
            t.start();
        }

        // Wait for them all to finish
        for (Thread t : threads) {
            t.join();
        }

        return estimatedPi[0]/5;
    }


    /**
     * Program entry point.
     */
    public static void main(String[] args) throws InterruptedException {
        new ExerciseThreeMultiThreaded().start();
    }
}
